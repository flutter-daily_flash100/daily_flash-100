import 'package:flutter/material.dart';

class five extends StatefulWidget {
  const five({super.key});

  State<five> createState() => fiveState();
}

class fiveState extends State<five> {
  bool _containerTapped = false;
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Padding(padding: EdgeInsets.only(left: 500, top: 390)),
          Container(
            height: 100,
            width: 100,
            color: _containerTapped ? Colors.blue : Colors.red,
          ),
          ElevatedButton(
              onPressed: () {
                setState(() {
                  _containerTapped = !_containerTapped;
                });
              },
              child: Text("submit"))
        ],
      ),
    );
  }
}
