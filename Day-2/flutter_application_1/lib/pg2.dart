import 'package:flutter/material.dart';

class second extends StatefulWidget {
  const second({super.key});

  State<second> createState() => secondState();
}

class secondState extends State<second> {
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Container(
        padding: EdgeInsets.all(15),
        child: Text(
          "welcome",
          style: TextStyle(color: Colors.black),
        ),
        height: 100,
        width: 100,
        decoration: BoxDecoration(
            color: const Color.fromARGB(255, 133, 199, 193),
            border: Border(left: BorderSide(width: 5, color: Colors.blue))),
      )),
    );
  }
}
