import 'package:flutter/material.dart';

class third extends StatefulWidget {
  const third({super.key});

  State<third> createState() => FirstState();
}

class FirstState extends State<third> {
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 200,
          width: 200,
          decoration: BoxDecoration(
              color: const Color.fromARGB(255, 148, 219, 212),
              border: Border.all(color: Colors.teal, width: 5),
              borderRadius: BorderRadius.only(topRight: Radius.circular(40))),
        ),
      ),
    );
  }
}
