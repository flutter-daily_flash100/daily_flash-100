import 'package:flutter/material.dart';

class four extends StatefulWidget {
  const four({super.key});

  State<four> createState() => FirstState();
}

class FirstState extends State<four> {
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 200,
          width: 400,
          decoration: BoxDecoration(
              color: const Color.fromARGB(255, 148, 219, 212),
              border: Border.all(color: Colors.teal, width: 5),
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(40),
                  bottomLeft: Radius.circular(40))),
          child: Text(
            "Welcome",
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
        ),
      ),
    );
  }
}
