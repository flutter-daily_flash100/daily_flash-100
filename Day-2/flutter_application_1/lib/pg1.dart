import 'package:flutter/material.dart';

class First extends StatefulWidget {
  const First({super.key});

  State<First> createState() => FirstState();
}

class FirstState extends State<First> {
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          margin: EdgeInsets.only(top: 50),
          height: 200,
          width: 200,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.red,
          ),
          padding: EdgeInsets.only(top: 90, left: 70),
          child: Text("rounak"),
        ),
      ),
    );
  }
}
