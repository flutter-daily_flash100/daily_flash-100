import 'package:flutter/material.dart';
import 'package:flutter_application_1/pg1.dart';
import 'package:flutter_application_1/pg3.dart';
import 'package:flutter_application_1/pg4.dart';
import 'package:flutter_application_1/pg5.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        body: five(),
      ),
    );
  }
}
