import 'package:flutter/material.dart';

class four extends StatefulWidget {
  const four({super.key});

  State<four> createState() => firstState();
}

class firstState extends State<four> {

  int x = 10;
  
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          decoration: BoxDecoration(
            color: Colors.blue,
            border: Border.all(
              color: Colors.red,
              width: 5,
            ),
          ),
        ),
      ),
    );
  }
}
//