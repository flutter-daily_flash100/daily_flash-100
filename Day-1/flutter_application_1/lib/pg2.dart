import 'package:flutter/material.dart';

class second extends StatefulWidget {
  const second({super.key});

  State<second> createState() => firstState();
}

class firstState extends State<second> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 153, 222, 215),
        centerTitle: true,
        title: const Text(
          "welcome",
          style: TextStyle(color: Colors.teal),
        ),
        actions: [
          IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.search,
              )),
          IconButton(onPressed: () {}, icon: Icon(Icons.menu)),
          IconButton(onPressed: () {}, icon: Icon(Icons.next_plan))
        ],
        leading:
            IconButton(onPressed: () {}, icon: Icon(Icons.notification_add)),
      ),
    );
  }
}
