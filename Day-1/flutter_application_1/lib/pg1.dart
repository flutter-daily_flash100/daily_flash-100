import 'package:flutter/material.dart';

class first extends StatefulWidget {
  const first({super.key});

  State<first> createState() => firstState();
}

class firstState extends State<first> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          "welcome",
          style: TextStyle(color: Colors.black),
        ),
        actions: [
          IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.search,
              )),
        ],
        leading:
            IconButton(onPressed: () {}, icon: Icon(Icons.notification_add)),
      ),
    );
  }
}
