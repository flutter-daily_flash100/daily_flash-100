import 'package:flutter/material.dart';

class five extends StatefulWidget {
  const five({super.key});

  State<five> createState() => firstState();
}

class firstState extends State<five> {
  int x = 10;

  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          decoration: BoxDecoration(
              color: Colors.blue,
              boxShadow: const [
                BoxShadow(
                    color: Colors.red, offset: Offset(2, 2), blurRadius: 5),
              ],
              borderRadius: BorderRadius.circular(30)),
        ),
      ),
    );
  }
}
//