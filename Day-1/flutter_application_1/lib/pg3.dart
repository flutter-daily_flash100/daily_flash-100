import 'package:flutter/material.dart';

class third extends StatefulWidget {
  const third({super.key});

  State<third> createState() => firstState();
}

class firstState extends State<third> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        centerTitle: true,
        title: const Text(
          "welcome",
          style: TextStyle(color: Colors.black),
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(bottom: Radius.circular(25))),
      ),
    );
  }
}
