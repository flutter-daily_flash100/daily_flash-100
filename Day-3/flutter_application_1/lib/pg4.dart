//create a container in center of the screen give the color you want and take the shadow to the container only upper side
import 'package:flutter/material.dart';

class four extends StatefulWidget {
  const four({super.key});

  State<four> createState() => firststate();
}

class firststate extends State<four> {
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Container(
        height: 300,
        width: 300,
        decoration: BoxDecoration(color: Colors.red, boxShadow: [
          BoxShadow(color: Colors.black, offset: Offset(0, -6), blurRadius: 5)
        ]),
      ),
    ));
  }
}
