//create a container in the center of the screen ,now in the background of the container display an image(the image can be asset or network )also text in the center of the container
import 'package:flutter/material.dart';

class Second extends StatefulWidget {
  const Second({super.key});

  State<Second> createState() => firststate();
}

class firststate extends State<Second> {
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
            height: 350,
            width: 350,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(
                    "https://w7.pngwing.com/pngs/435/115/png-transparent-valentines-day-greeting-card-gift-shriners-temple-love-blank-valentines-s-love-child-text.png"),
              ),
            ),
            child: Center(
              child: Container(
                child: Text(
                  "Rounak and Rudrani",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            )),
      ),
    );
  }
}
