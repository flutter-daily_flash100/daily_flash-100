//create a container in the center of the screen with size width=300,height=300 and give red border to the container when user tap the container change the color then color is red
import 'package:flutter/material.dart';

class third extends StatefulWidget {
  const third({super.key});

  State<third> createState() => firststate();
}

class firststate extends State<third> {
  Color flag = Colors.red;

  void getcolor() {
    setState(() {
      flag = Colors.green;
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
            onTap: () {
              getcolor();
            },
            child: Center(
              child: Container(
                height: 200,
                width: 200,
                decoration: BoxDecoration(
                  border: Border.all(color: flag, width: 5),
                ),
              ),
            )));
  }
}
