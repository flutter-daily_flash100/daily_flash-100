//create a container in the center of the screen with size width=300,height=300 and display an image in the center of the container apply appropriate padding to the container
import 'package:flutter/material.dart';

class five extends StatefulWidget {
  const five({super.key});

  State<five> createState() => firststate();
}

class firststate extends State<five> {
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Container(
      height: 300,
      width: 300,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          gradient: LinearGradient(
              begin: Alignment.bottomLeft,
              end: Alignment.bottomRight,
              colors: [Colors.red, Colors.red, Colors.blue, Colors.blue],
              stops: [0.0, 0.5, 0.5, 1])),
    )));
  }
}
