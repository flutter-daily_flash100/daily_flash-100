//create a container in the center of the screen with size width=300,height=300 and display an image in the center of the container apply appropriate padding to the container
import 'package:flutter/material.dart';

class first extends StatefulWidget {
  const first({super.key});

  State<first> createState() => firststate();
}

class firststate extends State<first> {
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Container(
        height: 300,
        width: 300,
        child: Image.network(
          "https://i.pinimg.com/originals/84/b8/5f/84b85f22b9fb0e6ba3e56b93df701bab.jpg",
        ),
      ),
    ));
  }
}
