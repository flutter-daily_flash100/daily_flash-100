import 'package:flutter/material.dart';

class fourth extends StatefulWidget {
  const fourth({super.key});

  State<fourth> createState() => firststate();
}

class firststate extends State<fourth> {
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(padding: EdgeInsets.only(left: 1000)),
          Container(
            height: 100,
            width: 100,
            color: Colors.red,
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            height: 100,
            width: 100,
            color: Colors.yellow,
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            height: 100,
            width: 100,
            color: Colors.green,
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
