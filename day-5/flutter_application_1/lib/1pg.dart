import 'package:flutter/material.dart';

class first extends StatefulWidget {
  const first({super.key});

  State<first> createState() => firststate();
}

class firststate extends State<first> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Color.fromARGB(255, 152, 224, 216),
        title: const Text(
          "Profile Information",
          style: TextStyle(
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.bold,
              fontSize: 20),
        ),
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 80),
            child: Image.network(
              "https://img.freepik.com/premium-vector/user-profile-icon-flat-style-member-avatar-vector-illustration-isolated-background-human-permission-sign-business-concept_157943-15752.jpg?size=338&ext=jpg&ga=GA1.1.1395880969.1709769600&semt=ais",
              height: 250,
              width: 250,
            ),
          ),
          Text(
            "Name:-Rounak Sanjay Bhaskarwar",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "Phone.no:-7083382204",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
          ),
        ],
      ),
    );
  }
}
