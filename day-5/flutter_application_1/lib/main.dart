import 'package:flutter/material.dart';
import 'package:flutter_application_1/1pg.dart';
import 'package:flutter_application_1/2pg.dart';
import 'package:flutter_application_1/3pg.dart';
import 'package:flutter_application_1/4pg.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        body: fourth(),
      ),
    );
  }
}
