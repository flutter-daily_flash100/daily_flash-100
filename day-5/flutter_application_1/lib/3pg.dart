import 'package:flutter/material.dart';

class Third extends StatefulWidget {
  const Third({Key? key});

  @override
  State<Third> createState() => FirstState();
}

class FirstState extends State<Third> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Image.network(
                "https://img.freepik.com/premium-vector/user-profile-icon-flat-style-member-avatar-vector-illustration-isolated-background-human-permission-sign-business-concept_157943-15752.jpg?size=338&ext=jpg&ga=GA1.1.1395880969.1709769600&semt=ais",
              ),
            ),
            SizedBox(height: 10),
            Container(
              child: Text(
                "Rounak",
                style: TextStyle(fontSize: 20),
              ),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.pink),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.blue, offset: Offset(2, 2), blurRadius: 5)
                  ],
                  borderRadius: BorderRadius.circular(10)),
            ),
          ],
        ),
      ),
    );
  }
}
